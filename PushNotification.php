0<?php

include("DBConnection.php");

/**
 * Method to use send push notification to android user.
 *
 * @param $message Message send for user
 * @param $deviceTokens Device Token used for Push Notification (GCM ID)
 *
 * @return bool Return True (Success) or False (Failure).
 */

// if need to send to multiple users value of $deviceTokens= array('','','')

function sendNotificationToAndroid($message, $deviceTokens, $title, $data)
{
// Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $result = explode(",", $deviceTokens);

    if(count($result)>1){
        $fields = array(
            'registration_ids' => $result,
            'notification' => array("text" => $message, "title" => $title),
            'data' => array('data' => array('data' => $data), 'isFrom' => 'Survey')

        );
    }
    else{
        $fields = array(
            'to' => $deviceTokens,
            'notification' => array("text" => $message, "title" => $title),
            'data' => array('data' => array('data' => $data), 'isFrom' => 'Survey')

        );
    }

    print_r(json_encode($fields));
    $headers = array(
        'Authorization:key=AIzaSyDKba6rfp36-gN54V2rNVvxb9oQtPkoIOE',
        'Content-Type: application/json'
    );

// Open connection
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

// Execute post
    $result = curl_exec($ch);

    print_r($result);

    if ($result === false) {
        return false;
    }

    curl_close($ch);

    return true;
}

//$fcmToken = $_POST['fcmToken'];
$message = $_POST['message'];
$questions = $_POST['questions'];
$options = $_POST['options'];
$title = $_POST['title'];


$newQuestions = json_decode($questions, true);
$newOptions = json_decode($options, true);

if (count($newQuestions) != count($newOptions)) {
    echo json_encode(array('status' => 0, 'message' => 'JSON data in request is not correct'));
} else {
    $i = 0;
    $jsonResponse;

    foreach ($newQuestions as $key) {
        $isValid = true;

        if ($key["questionType"] != '') {
            $questionType = $key['questionType'];
        } else {
            $isValid = false;
            break;
        }
        if ($key["question"] != '') {
            $question = $key['question'];
        } else {
            $isValid = false;
            break;
        }
        if ($isValid) {
            $sql = "INSERT INTO questions (questiontype, questionValue) VALUES ('" . $questionType . "', '" . $question . "')";
            $conn->exec($sql);
            $id = $conn->lastInsertId();
            if ($id != '') {
                $array = array($newOptions)[0][$i];
                $optionArray = array();
                foreach ($array as $newKey) {
                    if ($newKey != '') {
                        $sql = "INSERT INTO options (questionid, optionvalue) VALUES ('" . $id . "', '" . $newKey . "')";
                        $conn->exec($sql);
                        $optionID = $conn->lastInsertId();
                        $optionArray [] = array("optionId" => $optionID, "optionValue" => $newKey);
                    }
                }

                $jsonResponse[] = array("questionID" => $id, "questionType" => $questionType, "questionValue" => $question,
                    "options" => $optionArray);


                $i++;
            }


        }
    }

    $sql="SELECT fcmToken FROM users";
    $stmt=$conn->prepare($sql);
    $stmt->execute();
    $resul=$stmt->fetchAll(PDO::FETCH_ASSOC);

    $tokens = "";
    foreach($resul as $value){
        $tokens.=(($tokens=='')?'':',').$value['fcmToken'];
    }

    sendNotificationToAndroid($message, $tokens, $title, $jsonResponse);

}
?>
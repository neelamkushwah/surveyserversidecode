<?php


$deviceId = $_POST['deviceId'];
$fcmToken = $_POST['fcmToken'];

$response = array();

if ($deviceId != '' && $fcmToken != '') {
    //insert or update values in users table
    include("DBConnection.php");
    $sql = "SELECT * FROM users WHERE users.deviceID='" . $deviceId . "' ";

    $statement = $conn->prepare($sql);
    $statement->execute();
    $records = $statement->fetchAll(PDO::FETCH_ASSOC);

    if (count($records) > 0) {
        //update record code
        $sql = "UPDATE users SET users.fcmToken='" . $fcmToken . "' WHERE users.deviceID='" . $deviceId . "'";
        try {
            $conn->exec($sql);
            $response['status'] = 1;
            $response['message'] = "Record updated successfully";
        } catch (PDOException $e) {
            //error in update
            $response['status'] = 0;
            $response['message'] = "Something went wrong, error in update query";
        }
    } else {
        $sql = "INSERT INTO users (deviceID, fcmToken) VALUES ('" . $deviceId . "', '" . $fcmToken . "')";
        $conn->exec($sql);
        $id = $conn->lastInsertId();
        if ($id != '') {
            $response['status'] = 1;
            $response['message'] = "New record created successfully";
        } else {

            $response['status'] = 0;
            $response['message'] = "Something went wrong, error in insert query";
        }
    }
} else {
    $response['status'] = 0;
    $response['message'] = "Device id or FCM id can not be blank";
}

echo json_encode($response);

?>